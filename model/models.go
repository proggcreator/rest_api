package model

type Payment struct {
	Transaction  string `json:"transaction"`
	Currency     string `json:"currency"`
	Provider     string `json:"provider"`
	Bank         string `json:"bank"`
	Amount       int    `json:"amount"`
	PaymentDt    int    `json:"payment_dt"`
	DeliveryCost int    `json:"delivery_cost"`
	GoodsTotal   int    `json:"goods_total"`
}
type Items struct {
	Rid        string `json:"rid"`
	Name       string `json:"name"`
	Size       string `json:"size"`
	Brand      string `json:"brand"`
	ChrtID     int    `json:"chrt_id"`
	Price      int    `json:"price"`
	Sale       int    `json:"sale"`
	TotalPrice int    `json:"total_price"`
	NmID       int    `json:"nm_id"`
}

type Order struct {
	OrderUID          string  `json:"order_uid"`
	Entry             string  `json:"entry"`
	InternalSignature string  `json:"internal_signature"`
	Locale            string  `json:"locale"`
	CustomerID        string  `json:"customer_id"`
	TrackNumber       string  `json:"track_number"`
	DeliveryService   string  `json:"delivery_service"`
	Shardkey          string  `json:"shardkey"`
	Items             []Items `json:"items"`
	Payment           Payment `json:"payment"`
	SmID              int     `json:"sm_id"`
}

type OrderShortAns struct {
	OrderUID        string `json:"order_uid"`
	Entry           string `json:"entry"`
	CustomerID      string `json:"customer_id"`
	TrackNumber     string `json:"track_number"`
	DeliveryService string `json:"delivery_service"`
	TotalPrice      int    `json:"total_price"`
}

func (o *Order) ConvertOrderToShortAns() (or *OrderShortAns) {
	var totalPrice int = o.Payment.GoodsTotal + o.Payment.DeliveryCost

	return &OrderShortAns{
		OrderUID:        o.OrderUID,
		Entry:           o.Entry,
		TotalPrice:      totalPrice,
		CustomerID:      o.CustomerID,
		TrackNumber:     o.TrackNumber,
		DeliveryService: o.DeliveryService,
	}
}

package service

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/proggcreator/rest_api/model"
)

type ordersInterface interface {
	GetOrder(string) (*model.OrderShortAns, error)
	AddOrder(*model.Order) error
}

func (svc *service) GetOrder(uid string) (*model.OrderShortAns, error) {
	order, err := svc.storage.GetOrder(uid)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	return order, nil
}

func (svc *service) AddOrder(order *model.Order) error {
	if err := svc.storage.AddOrder(order); err != nil {
		return err
	}
	return nil
}

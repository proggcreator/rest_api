package service

import "gitlab.com/proggcreator/rest_api/internal/repository"

type service struct {
	storage repository.RepositoryInterface
}

type ServiceInterface interface {
	ordersInterface
}

func NewStoreService(storage repository.RepositoryInterface) ServiceInterface {
	return &service{
		storage: storage,
	}
}

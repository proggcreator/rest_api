package nats

import (
	"encoding/json"
	"time"

	nats "github.com/nats-io/nats.go"
	stan "github.com/nats-io/stan.go"
	"github.com/sirupsen/logrus"
	"gitlab.com/proggcreator/rest_api/internal/service"
	"gitlab.com/proggcreator/rest_api/model"
)

type natsStruct struct {
	service service.ServiceInterface
}

func stanSubscribe(sc stan.Conn, cfg *NatsConf, srv service.ServiceInterface) error {

	natsService := &natsStruct{
		service: srv,
	}

	//stan subscribe with option
	_, err := sc.Subscribe(cfg.SubName,
		natsService.messageHandler,
		stan.DeliverAllAvailable(),
		stan.SetManualAckMode(),
		stan.AckWait(time.Second*5),
		stan.DurableName(cfg.SubType))
	return err
}

func NewConnection(cfg *NatsConf, srv service.ServiceInterface) (*nats.Conn, stan.Conn, error) {
	options := []nats.Option{}
	// connect to nats
	nc, err := nats.Connect(cfg.Server, options...)
	if err != nil {
		logrus.Fatalf("Error of nats connecting %v", err)
		return nil, nil, err
	}

	//pass to stan connection, callback reason
	sc, err := stan.Connect(cfg.ClusterID, cfg.ClientID, stan.NatsConn(nc),
		stan.SetConnectionLostHandler(func(_ stan.Conn, reason error) {
			logrus.Fatalf("Connection lost, reason: %v", reason)
		}))
	if err != nil {
		return nil, nil, err
	}
	//subscribing
	err = stanSubscribe(sc, cfg, srv)
	if err != nil {
		logrus.Infof("Error of subscribing %v", err)
	}

	logrus.Infof("Successfully subscribed")

	return nc, sc, nil
}

//function for nats subscribe
func (ns natsStruct) messageHandler(msg *stan.Msg) {
	order := &model.Order{}
	//fmt.Println("*******************************")
	//fmt.Println(string(msg.Data))
	if err := json.Unmarshal(msg.Data, &order); err != nil {
		logrus.Warnf("Error unmarshaling: %vs", msg.Data)
	} else {
		err = ns.service.AddOrder(order)
		if err != nil {
			logrus.Errorf("Error adding order from Nats: %v", err)
			return
		}
	}
	//check ack
	if err := msg.Ack(); err != nil {
		logrus.Errorf("Error of Nats Ack: %v", err)
		return
	}
}

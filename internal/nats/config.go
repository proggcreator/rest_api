package nats

type NatsConf struct {
	Server    string
	ClusterID string
	ClientID  string
	SubName   string
	SubType   string
}

package repository

import (
	"encoding/json"

	"github.com/sirupsen/logrus"
	"gitlab.com/proggcreator/rest_api/model"
)

type RepositoryInterface interface {
	AddOrder(*model.Order) error
	GetOrder(uid string) (*model.OrderShortAns, error)
	UpdateCache() error
	Close() error
}

type Repository struct {
	store *Store
}

func NewRepository(store *Store) *Repository {
	return &Repository{
		store: store,
	}
}

type OrderAnsBytes []uint8

func (r *Repository) AddOrder(order *model.Order) error {
	orderShortJson, err := json.Marshal(order.ConvertOrderToShortAns())
	if err != nil {
		return err
	}

	orderJson, err := json.Marshal(order)
	if err != nil {
		logrus.Errorf("Error of order marshaling: %v", err)
		return err
	}
	tx, err := r.store.db.Begin()
	if err != nil {
		return err
	}

	var count int
	if err = tx.QueryRow("SELECT COUNT(*) FROM orders WHERE id = $1", order.OrderUID).Scan(&count); err != nil {
		return err
	}

	//check unique order for repository
	if count == 0 {
		if _, err = tx.Exec(
			"INSERT INTO orders (id, data, short) VALUES ($1, $2, $3)",
			order.OrderUID, orderJson, orderShortJson); err != nil {
			tx.Rollback()
			logrus.Errorf("Can't add order with uid %s: %v", order.OrderUID, err)
			return err
		}
	}
	err = tx.Commit()
	if err != nil {
		logrus.Errorf("Error commit transaction: %v", err)
		return err
	}

	err = r.store.cache.Set(order.ConvertOrderToShortAns())
	if err != nil {
		logrus.Errorf("Can't add order with uid %s to cache: %v", order.OrderUID, err)
	}
	return nil
}

func (r *Repository) GetOrder(uid string) (*model.OrderShortAns, error) {
	//search in cache
	o, err := r.store.cache.Get(uid)
	if err != nil {
		logrus.Errorf("Can't find order %s to cache: %v", o.OrderUID, err)
		return o, err
	}
	//search in db
	order := new(model.OrderShortAns)
	var ansBytes OrderAnsBytes
	if err := r.store.db.QueryRow("SELECT short FROM orders WHERE id = $1", uid).Scan(&ansBytes); err != nil {
		return nil, err
	}

	if err := json.Unmarshal(ansBytes, &order); err != nil {
		return nil, err
	}

	return order, nil
}

func (r *Repository) UpdateCache() error {
	rows, err := r.store.db.Query("SELECT short FROM orders;")
	if err != nil {
		return err
	}
	var ansBytes OrderAnsBytes
	for rows.Next() {
		if err := rows.Scan(&ansBytes); err != nil {
			return err
		}
		ansOrder := new(model.OrderShortAns)
		if err := json.Unmarshal(ansBytes, &ansOrder); err != nil {
			return err
		}
		//check unique for cache
		if err := r.store.cache.Set(ansOrder); err != nil {
			logrus.Infof(" can't add order with uid %s to cache: %v", ansOrder.OrderUID, err)
		}
	}
	return nil
}

func (r *Repository) Close() error {
	return r.store.db.Close()
}

package repository

import (
	"database/sql"

	"gitlab.com/proggcreator/rest_api/internal/cache"
)

type Store struct {
	db    *sql.DB
	cache *cache.Cache
}

func NewStore(db *sql.DB) *Store {
	return &Store{
		db:    db,
		cache: cache.NewCache(),
	}
}

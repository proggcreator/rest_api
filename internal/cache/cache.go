package cache

import (
	"errors"
	"sync"

	"gitlab.com/proggcreator/rest_api/model"
)

type Cache struct {
	cache map[string]*model.OrderShortAns
	sync.RWMutex
}

func NewCache() *Cache {
	return &Cache{
		cache: make(map[string]*model.OrderShortAns),
	}
}

//get order from cache
func (c *Cache) Get(id string) (order *model.OrderShortAns, err error) {
	c.Lock()

	if el, ok := c.cache[id]; ok {
		c.Unlock()
		return el, nil
	}
	errCache := errors.New("the order in cache not exists")
	c.Unlock()
	return &model.OrderShortAns{}, errCache
}

//set new order in cache
func (c *Cache) Set(order *model.OrderShortAns) (err error) {
	c.Lock()

	if _, ok := c.cache[order.OrderUID]; ok {
		c.Unlock()
		return errors.New("the order in cache already exists")
	}
	c.cache[order.OrderUID] = order
	c.Unlock()

	return nil
}

package main

import (
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"

	"github.com/spf13/viper"
	"gitlab.com/proggcreator/rest_api/configs"
	"gitlab.com/proggcreator/rest_api/internal/nats"
	"gitlab.com/proggcreator/rest_api/internal/repository"
	"gitlab.com/proggcreator/rest_api/internal/service"
	"gitlab.com/proggcreator/rest_api/server"
)

func main() {
	logrus.SetFormatter(new(logrus.JSONFormatter)) //формат для логгера json
	if err := initConfig(); err != nil {
		logrus.Fatalf("error initializing configs")
	}

	cfg := &configs.ConfApi{
		Portapi:    viper.GetString("api.serverapi.port"),
		Portstatic: viper.GetString("api.serverstatic.port"),
	}

	//create database
	db, err := repository.NewPostgresDB(repository.Config{
		Host:     viper.GetString("db.host"),
		Port:     viper.GetString("db.port"),
		Username: viper.GetString("db.username"),
		Password: viper.GetString("db.password"),
		DBName:   viper.GetString("db.dbname"),
		SSLMode:  viper.GetString("db.sslmode"),
	})
	if err != nil {
		logrus.Fatalf("faled to initialization %s", err.Error())
	}

	//create store
	store := repository.NewStore(db)
	//create repository
	repository := repository.NewRepository(store)
	defer repository.Close()

	//create service
	service := service.NewStoreService(repository)
	if err != nil {
		logrus.Fatalf("Can't create new service: %v", err)
	}

	//create nats
	nats, stan, err := nats.NewConnection(&nats.NatsConf{
		Server:    viper.GetString("nats.server"),
		ClusterID: viper.GetString("nats.clusterId"),
		ClientID:  viper.GetString("nats.clientId"),
		SubName:   viper.GetString("nats.subName"),
		SubType:   viper.GetString("nats.subType"),
	}, service)
	if err != nil {
		logrus.Fatalf("Can't connect to nats: %v", err)
	}
	defer stan.Close()
	defer nats.Close()

	if err = repository.UpdateCache(); err != nil {
		logrus.Fatalf("Can't update cache %v", err)
	}
	//create static server
	staticServer, staticErrors := server.NewStatic(cfg, service)
	defer func() {
		if err = staticServer.Shutdown(); err != nil {
			logrus.Fatalf("Can't shutdown static server: %v", err)
		}
	}()

	//create apiserver
	apiServer, apiErrors := server.NewAPI(cfg, service)
	defer func() {
		if err = apiServer.Shutdown(); err != nil {
			logrus.Fatalf("Can't shutdown API server: %v", err)
		}
	}()

	select {
	case err = <-staticErrors:
		logrus.Fatalf("Static server error: %v", err)
	case err = <-apiErrors:
		logrus.Fatalf("API server error: %v", err)
	}

}
func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()

}

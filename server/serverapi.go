package server

import (
	"encoding/json"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
	"gitlab.com/proggcreator/rest_api/configs"
	"gitlab.com/proggcreator/rest_api/internal/service"
)

func cors(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		ctx.Response.Header.Set("Access-Control-Allow-Headers", "Content-Type,Accept")
		ctx.Response.Header.Set("Access-Control-Allow-Methods", "OPTIONS,POST,GET")
		ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
		if string(ctx.Method()) == fasthttp.MethodOptions {
			ctx.Response.SetStatusCode(200)
			return
		}
		next(ctx)
	}
}

func NewAPI(cfg *configs.ConfApi, srv service.ServiceInterface) (*fasthttp.Server, chan error) {
	errChan := make(chan error, 1)

	server := &fasthttp.Server{
		ReadTimeout: time.Second * 3,
		Handler:     cors(handlerAPI(srv)),
	}

	//starting server
	go func() {
		errChan <- server.ListenAndServe(cfg.Portapi)
	}()

	logrus.Infof("API server running... %s", cfg.Portapi)

	return server, errChan
}

func handlerAPI(srv service.ServiceInterface) func(ctx *fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		path, method := string(ctx.Path()), string(ctx.Method())
		switch {
		case path == "/order" && method == fasthttp.MethodGet:
			uid := string(ctx.QueryArgs().Peek("order_uid"))

			if len(uid) == 0 {
				logrus.Errorf("Error return querry arg")
				ctx.SetStatusCode(fasthttp.StatusBadRequest)
				return
			}

			order, err := srv.GetOrder(uid)
			if err != nil {
				ctx.SetStatusCode(fasthttp.StatusNotFound)
				return
			}

			ans, err := json.Marshal(order)
			if err != nil {
				ctx.SetStatusCode(fasthttp.StatusInternalServerError)
				return
			}

			_, err = ctx.Write(ans)
			if err != nil {
				ctx.SetStatusCode(fasthttp.StatusNotFound)
				return
			}
			ctx.SetStatusCode(fasthttp.StatusOK)
		default:
			ctx.SetStatusCode(fasthttp.StatusNotFound)
		}
	}
}

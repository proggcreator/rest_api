package server

import (
	"html/template"
	"time"

	"gitlab.com/proggcreator/rest_api/configs"
	"gitlab.com/proggcreator/rest_api/internal/service"

	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
)

func NewStatic(cfg *configs.ConfApi, srv service.ServiceInterface) (*fasthttp.Server, chan error) {
	errChan := make(chan error, 1)

	server := &fasthttp.Server{
		ReadTimeout: time.Second * 3,
		Handler:     handlerStatic(cfg),
	}

	go func() {
		errChan <- server.ListenAndServe(cfg.Portstatic)
	}()

	logrus.Infof("Static server running...")

	return server, errChan
}

func handlerStatic(cfg *configs.ConfApi) func(ctx *fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		path, method := string(ctx.Path()), string(ctx.Method())
		switch {

		case path == "/find" && method == fasthttp.MethodGet:
			tmpl, err := template.ParseFiles("./internal/static/index.html")
			if err != nil {
				ctx.SetStatusCode(fasthttp.StatusNotFound)
				return
			}
			err = tmpl.Execute(ctx.Response.BodyWriter(), cfg)
			if err != nil {
				ctx.SetStatusCode(fasthttp.StatusNotFound)
				return
			}

			ctx.Response.Header.Add("Content-Type", "text/html; charset=utf-8")
		default:
			ctx.SetStatusCode(fasthttp.StatusNotFound)
		}
	}
}

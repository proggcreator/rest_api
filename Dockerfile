FROM golang:alpine

ENV GO111MODULE=on

RUN apk update && apk add ca-certificates

ADD configs/config.go configs/config.go

ADD orderapp orderapp

RUN chmod +x orderapp

ENTRYPOINT [ "/orderapp" ]

module gitlab.com/proggcreator/rest_api

go 1.16

require (
	github.com/lib/pq v1.10.3
	github.com/nats-io/nats-streaming-server v0.23.0 // indirect
	github.com/nats-io/nats.go v1.13.0
	github.com/nats-io/stan.go v0.10.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.9.0
	github.com/valyala/fasthttp v1.31.0
)

CREATE TABLE orders
(
    order_uid          varchar(255) PRIMARY KEY,    
	entry             varchar(255) not null,
	internal_signature varchar(255) not null, 
	locale            varchar(255) not null,
	customer_id        varchar(255) not null,
	track_number       varchar(255) not null,
	delivery_service   varchar(255) not null,
	shardkey          varchar(255) not null,
	sm_id              integer not null  

);

CREATE TABLE items 
(
	order_uid    varchar(255) references orders(order_uid),
    chrt_id     integer not null,     
	price      integer not null,    
	rid        varchar(255) not null,
	name       varchar(255) not null,
	sale       integer not null,    
	size       varchar(255) not null, 
	total_price integer not null,    
	nm_id       integer not null,  
	brand      varchar(255) not null
);

CREATE TABLE payments 
(
    order_uid    varchar(255) references orders(order_uid), 
    transaction  varchar(255) not null,
	currency     varchar(255) not null,
	provider     varchar(255) not null,
	amount       integer not null, 
	payment_dt   integer not null, 
	bank         varchar(255) not null,
	delivery_cost integer not null, 
	goods_total   integer not null
);



CREATE TABLE orders
(
    id      varchar not null unique,
    data    json    not null,
    short   json    not null
);
